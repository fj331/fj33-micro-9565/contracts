package contracts.users

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    request {
        method GET()
        url '/me'
        headers {
            accept applicationJson()
        }
    }

    response {
        status OK()
        headers {
            contentType applicationJson()
        }
        body (
            id: value(anyUuid()),
            username: "username"
        )
    }
}